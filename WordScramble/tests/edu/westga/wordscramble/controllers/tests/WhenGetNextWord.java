package edu.westga.wordscramble.controllers.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import edu.westga.wordscramble.controllers.GetNextWordController;
import edu.westga.wordscramble.model.DataMap;
import edu.westga.wordscramble.model.ScrambledWord;
import edu.westga.wordscramble.model.WordDataMap;

/**
 * Tests the Get Next Word usecase
 * 
 * @author Nathan Beam
 * @version Spring 2015
 */
public class WhenGetNextWord {
	
	/**
	 * no-op controller
	 */
	public WhenGetNextWord() {
		//no-op
	}
	
	/**
	 * Test method for
	 * {@link edu.westga.wordscramble.controllers.GetNextWord#getNextWord()}
	 */
	@Test
	public void shouldChooseSixLetWhenGameModeEqualsSix() {
		DataMap <String, ScrambledWord> testMap = new WordDataMap();
		ArrayList<String> testArrayList = new ArrayList<String>();
		
		ScrambledWord scrambledWord1 = new ScrambledWord("Lorem");
		ScrambledWord scrambledWord2 = new ScrambledWord("dolor");
		ScrambledWord scrambledWord3 = new ScrambledWord("amet");
		ScrambledWord scrambledWord4 = new ScrambledWord("SixLet");

		testMap.add(scrambledWord1.getUnscrambledWord(), scrambledWord1);
		testMap.add(scrambledWord2.getUnscrambledWord(), scrambledWord2);
		testMap.add(scrambledWord3.getUnscrambledWord(), scrambledWord3);
		testMap.add(scrambledWord4.getUnscrambledWord(), scrambledWord4);
		
		testArrayList.add(scrambledWord1.getUnscrambledWord());
		testArrayList.add(scrambledWord2.getUnscrambledWord());
		testArrayList.add(scrambledWord3.getUnscrambledWord());
		testArrayList.add(scrambledWord4.getUnscrambledWord());
		
		GetNextWordController testController = new GetNextWordController(testMap, testArrayList);
		
		assertEquals("SixLet", testController.getNextWord(6).getUnscrambledWord());
	}
	
	/**
	 * Test method for
	 * {@link edu.westga.wordscramble.controllers.GetNextWord#getNextWord()}
	 */
	@Test
	public void shouldChooseLoremWhenGameModeEquals5() {
		DataMap <String, ScrambledWord> testMap = new WordDataMap();
		ArrayList<String> testArrayList = new ArrayList<String>();
		
		ScrambledWord scrambledWord1 = new ScrambledWord("lorem");
		ScrambledWord scrambledWord2 = new ScrambledWord("dopolor");
		ScrambledWord scrambledWord3 = new ScrambledWord("amet");
		ScrambledWord scrambledWord4 = new ScrambledWord("SixLet");

		testMap.add(scrambledWord1.getUnscrambledWord(), scrambledWord1);
		testMap.add(scrambledWord2.getUnscrambledWord(), scrambledWord2);
		testMap.add(scrambledWord3.getUnscrambledWord(), scrambledWord3);
		testMap.add(scrambledWord4.getUnscrambledWord(), scrambledWord4);
		
		testArrayList.add(scrambledWord1.getUnscrambledWord());
		testArrayList.add(scrambledWord2.getUnscrambledWord());
		testArrayList.add(scrambledWord3.getUnscrambledWord());
		testArrayList.add(scrambledWord4.getUnscrambledWord());
		
		GetNextWordController testController = new GetNextWordController(testMap, testArrayList);
		
		assertEquals("lorem", testController.getNextWord(5).getUnscrambledWord());
	}
	
	/**
	 * Test method for
	 * {@link edu.westga.wordscramble.controllers.GetNextWord#getNextWord()}
	 */
	@Test
	public void shouldChooseAmetWhenGameModeEqualsRandom() {
		DataMap <String, ScrambledWord> testMap = new WordDataMap();
		ArrayList<String> testArrayList = new ArrayList<String>();
		
		ScrambledWord scrambledWord1 = new ScrambledWord("amet");

		testMap.add(scrambledWord1.getUnscrambledWord(), scrambledWord1);
		
		testArrayList.add(scrambledWord1.getUnscrambledWord());
		
		GetNextWordController testController = new GetNextWordController(testMap, testArrayList);
		
		assertEquals("amet", testController.getNextWord(0).getUnscrambledWord());
	}
	
	/**
	 * Test method for
	 * {@link edu.westga.wordscramble.controllers.GetNextWord#getNextWord()}
	 */
	@Test
	public void shoulHaveSpaceAtIndex6WhenGameModeEqualsDouble() {
		DataMap <String, ScrambledWord> testMap = new WordDataMap();
		ArrayList<String> testArrayList = new ArrayList<String>();
		
		ScrambledWord scrambledWord1 = new ScrambledWord("SixLet");
		ScrambledWord scrambledWord2 = new ScrambledWord("LetSix");

		testMap.add(scrambledWord1.getUnscrambledWord(), scrambledWord1);
		testMap.add(scrambledWord2.getUnscrambledWord(), scrambledWord1);

		
		testArrayList.add(scrambledWord1.getUnscrambledWord());
		testArrayList.add(scrambledWord2.getUnscrambledWord());

		GetNextWordController testController = new GetNextWordController(testMap, testArrayList);
		
		assertTrue(testController.getNextWord(-1).getScrambledWord().indexOf(" ") == 6);
	}
	
	

}
