package edu.westga.wordscramble.controllers.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import edu.westga.wordscramble.controllers.LoadWordDataFromLocalFileController;
import edu.westga.wordscramble.model.DataMap;
import edu.westga.wordscramble.model.ScrambledWord;
import edu.westga.wordscramble.model.WordDataMap;


/**
 * Tests the Load Word Data From Local File usecase
 * 
 * @author Nathan Beam
 * @version Spring 2015 *
 */
public class WhenLoadWordDataFromLocalFile {

	private LoadWordDataFromLocalFileController testLoader;
	private ArrayList<String> testArrayList;
	private DataMap<String, ScrambledWord> testMap;
	
	
	/**
	 *No-op constructor 
	 */
	public WhenLoadWordDataFromLocalFile() {
		//no-op
	}
	
	/**
	 * Sets up the Loader for testing  
	 */
	@Before
	public void setUp() {
		File testFile = new File("testFiles/testDocument.txt");
		this.testMap = new WordDataMap();
		this.testLoader = new LoadWordDataFromLocalFileController(testFile, this.testMap);
		try {
			this.testLoader.loadWordData();
		} catch (IOException exception) {
			// TODO Auto-generated catch block
			exception.printStackTrace();
		}
		this.testArrayList = this.testLoader.getUnscrambledWordsArrayList();
	}
	
	/**
	 * Test method for
	 * {@link edu.westga.wordscramble.controllers.LoadWordDataFromLocalFileController#loadWordData()}
	 */
	@Test
	public void firstWordInArrayListShouldBeLorem() {
		assertEquals("lorem", this.testArrayList.get(0));
	}
	
	/**
	 * Test method for
	 * {@link edu.westga.wordscramble.controllers.LoadWordDataFromLocalFileController#loadWordData()}
	 */
	@Test
	public void lastWordInArrayListShouldBeLaborum() {
		assertEquals("laborum", this.testArrayList.get(this.testArrayList.size() - 1));
	}
	
	/**
	 * Test method for
	 * {@link edu.westga.wordscramble.controllers.LoadWordDataFromLocalFileController#loadWordData()}
	 */
	@Test
	public void dataMapShouldContainFirstWordInTestFile() {
		assertTrue(this.testMap.contains(this.testArrayList.get(0)));
	}
	
	/**
	 * Test method for
	 * {@link edu.westga.wordscramble.controllers.LoadWordDataFromLocalFileController#loadWordData()}
	 */
	@Test
	public void dataMapShouldContainlastWordInTestFile() {
		assertTrue(this.testMap.contains(this.testArrayList.get(this.testArrayList.size() - 1)));
	}
}
