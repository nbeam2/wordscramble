package edu.westga.wordscramble.controllers.tests;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import edu.westga.wordscramble.controllers.LoadWordDataFromUrlController;
import edu.westga.wordscramble.model.DataMap;
import edu.westga.wordscramble.model.ScrambledWord;
import edu.westga.wordscramble.model.WordDataMap;


/**
 * Tests the Load Word Data From remote File usecase
 * 
 * @author Nathan Beam
 * @version Spring 2015 *
 */
public class WhenLoadWordDataFromUrl {

	private LoadWordDataFromUrlController testLoader;
	private ArrayList<String> testArrayList;
	private DataMap<String, ScrambledWord> testMap;
	private static final String TEST_URL_STRING 
							= "http://stu.westga.edu/~nbeam2/javaproject/mobydick2.txt";
	
	
	/**
	 *No-op constructor 
	 */
	public WhenLoadWordDataFromUrl() {
		//no-op
	}
	
	/**
	 * Sets up the Loader for testing  
	 */
	@Before
	public void setUp() {
		
		this.testMap = new WordDataMap();
		this.testLoader = new LoadWordDataFromUrlController(TEST_URL_STRING, this.testMap);
		try {
			this.testLoader.loadWordData();
		} catch (IOException exception) {
			// TODO Auto-generated catch block
			exception.printStackTrace();
		}
		this.testArrayList = this.testLoader.getUnscrambledWordsArrayList();
	}
	
	/**
	 * Test method for
	 * {@link edu.westga.wordscramble.controllers.LoadWordDataFromUrlController#loadWordData()}
	 * .
	 */
	@Test
	public void firstWordInArrayListShouldBeStuffed() {
		assertEquals("stuffed", this.testArrayList.get(0));
	}
	
	/**
	 * Test method for
	 * {@link edu.westga.wordscramble.controllers.LoadWordDataFromUrlController#loadWordData()}
	 * .
	 */
	@Test
	public void lastWordInArrayListShouldBeParticular() {
		assertEquals("particular", this.testArrayList.get(this.testArrayList.size() - 1));
	}
	
	/**
	 * Test method for
	 * {@link edu.westga.wordscramble.controllers.LoadWordDataFromUrlController#loadWordData()}
	 * .
	 */
	@Test
	public void dataMapShouldContainFirstWordInTestFile() {
		assertTrue(this.testMap.contains(this.testArrayList.get(0)));
	}
	
	/**
	 * Test method for
	 * {@link edu.westga.wordscramble.controllers.LoadWordDataFromUrlController#loadWordData()}
	 * .
	 */
	@Test
	public void dataMapShouldContainlastWordInTestFile() {
		assertTrue(this.testMap.contains(this.testArrayList.get(this.testArrayList.size() - 1)));
	}
}
