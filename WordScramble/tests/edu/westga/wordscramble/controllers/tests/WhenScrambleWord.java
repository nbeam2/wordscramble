package edu.westga.wordscramble.controllers.tests;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

import edu.westga.wordscramble.controllers.WordScramblingController;

/**
 * Tests the Scramble Word usecase
 * 
 * @author Nathan Beam
 * @version Spring 2015
 *
 */
public class WhenScrambleWord {

	
	/**
	 * no-op array 
	 */
	public WhenScrambleWord() {
		//no-op
	}

	/**
	 * Test method for
	 * {@link edu.westga.wordscramble.controllers.WhenScrambleWord#getScrambledWord()}
	 */
	@Test
	public void returnedScrarmbledWordShouldNotEqualOriginalWord() {
		WordScramblingController testController = new WordScramblingController("nathan");

		assertFalse("nathan".equals(testController.getScrambledWord()));
	}
	
	/**
	 * Test method for
	 * {@link edu.westga.wordscramble.controllers.WhenScrambleWord#getScrambledWord()}
	 */
	@Test
	public void returnedScrambledWordShouldHaveSameCharsAsOriginalWord() {
		WordScramblingController testController = new WordScramblingController("nathan");
		char[] testArray = testController.getScrambledWord().toCharArray();
		char[] nathanArray = "nathan".toCharArray();
		Arrays.sort(testArray);
		Arrays.sort(nathanArray);
		assertTrue(Arrays.equals(testArray, nathanArray));
	}
	
	/**
	 * Test method for
	 * {@link edu.westga.wordscramble.controllers.WhenScrambleWord#doubleScramble()}
	 */
	@Test
	public void returnedDoubleScrarmbledWordShouldNotEqualOriginalWord() {
		WordScramblingController testController = new WordScramblingController("nathan beam");

		assertFalse("nathan beam".equals(testController.getScrambledWord()));
	}
	
	/**
	 * Test method for
	 * {@link edu.westga.wordscramble.controllers.WhenScrambleWord#doubleScramble()}
	 */
	@Test
	public void returnedDoubleScrarmbledWordShouldHaveSameCharsAsOriginalWord() {
		WordScramblingController testController = new WordScramblingController("nathan beam");
		char[] testArray = testController.getScrambledWord().toCharArray();
		char[] nathanArray = "nathan beam".toCharArray();
		Arrays.sort(testArray);
		Arrays.sort(nathanArray);
		assertTrue(Arrays.equals(testArray, nathanArray));
	}

}
