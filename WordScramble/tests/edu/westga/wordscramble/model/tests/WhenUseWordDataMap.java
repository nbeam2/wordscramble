package edu.westga.wordscramble.model.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.westga.wordscramble.model.DataMap;
import edu.westga.wordscramble.model.ScrambledWord;
import edu.westga.wordscramble.model.WordDataMap;

/**
 * Test Class for the When Use Word Data Map usecase
 * 
 * @author Nathan Beam
 * @version Spring 2015
 *
 */
public class WhenUseWordDataMap {

	private DataMap<String, ScrambledWord> testMap;
	
	/**
	 *no-op constructor 
	 */
	public WhenUseWordDataMap() {
		//no-op
	}
	
	/**
	 * Sets up the map for testing 
	 */
	@Before
	public void setUp() {
		this.testMap = new WordDataMap();
		ScrambledWord testWord1 = new ScrambledWord("lorem");
		ScrambledWord testWord2 = new ScrambledWord("ipsum");

		this.testMap.add(testWord1.getUnscrambledWord(), testWord1);
		this.testMap.add(testWord2.getUnscrambledWord(), testWord2);

	}
	
	/**
	 * Test method for
	 * {@link edu.westga.wordscramble.model.WordDataMap#contains()}
	 */
	@Test
	public void shouldFindFirstWordAdded() {
		assertTrue(this.testMap.contains("lorem"));
	}
	
	/**
	 * Test method for
	 * {@link edu.westga.wordscramble.model.WordDataMap#contains()}
	 */
	@Test
	public void shouldFindSecondWordAdded() {
		assertTrue(this.testMap.contains("ipsum"));
	}
	
	/**
	 * Test method for
	 * {@link edu.westga.wordscramble.model.WordDataMap#contains()}
	 */
	@Test
	public void shouldNotFindWordNotAdded() {
		assertFalse(this.testMap.contains("dolor"));
	}

}
