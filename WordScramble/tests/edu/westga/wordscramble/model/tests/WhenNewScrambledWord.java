package edu.westga.wordscramble.model.tests;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import edu.westga.wordscramble.model.ScrambledWord;

/**
 * Test class for the When New Scrambled Word usecase
 * 
 * @author Nathan Beam
 * @version Spring 2015
 *
 */
public class WhenNewScrambledWord {

	
	/**
	 * no-op constructor 
	 */
	public WhenNewScrambledWord() {
		//no-op
	}
	private ScrambledWord testWord;
	
	/**
	 * Sets up the word for testing
	 */
	@Before
	public void setUp() {
		this.testWord = new ScrambledWord("lorem");
	}
	
	/**
	 * Test method for
	 * {@link edu.westga.wordscramble.model.ScrambledWord#getUnscrambledWord()}
	 */
	@Test
	public void unscrambledWordShouldBeLorem() {
		assertEquals("lorem", this.testWord.getUnscrambledWord());
	}

	/**
	 * Test method for
	 * {@link edu.westga.wordscramble.model.ScrambledWord#getScrambledWord()}
	 */
	@Test
	public void scrambledWordShouldNotBeLorem() {
		assertFalse("lorem".equals(this.testWord.getScrambledWord()));
	}
	
	/**
	 * Test method for
	 * {@link edu.westga.wordscramble.model.ScrambledWord#getScrambledWord()}
	 */
	@Test
	public void scrambledWordShouldHaveSameCharsAsLorem() {
		char[] testArray = this.testWord.getScrambledWord().toCharArray();
		char[] loremArray = "lorem".toCharArray();
		Arrays.sort(testArray);
		Arrays.sort(loremArray);
		assertTrue(Arrays.equals(testArray, loremArray));
	}
	
	/**
	 * Test method for
	 * {@link edu.westga.wordscramble.model.ScrambledWord#getWordLength()}
	 */
	@Test
	public void wordLengthShouldBe5() {
		assertEquals(5, this.testWord.getWordLength());
	}
	

}
