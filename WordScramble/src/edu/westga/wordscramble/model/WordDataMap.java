package edu.westga.wordscramble.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * WordDataMap implements the DataMap interface to 
 * store String/ScrambledWord pairs in memory.
 * 
 * @author Nathan Beam
 * @version Spring, 2015
 */
public class WordDataMap implements DataMap<String, ScrambledWord> {
	
	private Map<String, ScrambledWord> theMap;
	
	/**
	 * Creates a new WordDataMap with an empty map.
	 * 
	 * @precondition none
	 * @postcondition size() == 0
	 */
	public WordDataMap() {
		this.theMap = new HashMap<>();
	}

	@Override
	public void add(String aKey, ScrambledWord aValue) {
		if (aKey == null) {
			throw new IllegalArgumentException("The key was null");
		}
		if (aValue == null) {
			throw new IllegalArgumentException("The value was null");
		}
		if (this.contains(aKey)) {
			throw new IllegalArgumentException("Key value is already in the map");
		}
		
		this.theMap.put(aKey, aValue);		
	}
		

	@Override
	public ScrambledWord get(String aKey) {
		if (aKey == null) {
			throw new IllegalArgumentException("The key was null");
		}
		if (!this.contains(aKey)) {
			throw new IllegalArgumentException("Key value is not in the map");
		}
		
		return this.theMap.get(aKey);
	}

	@Override
	public boolean contains(String aKey) {
		if (aKey == null) {
			throw new IllegalArgumentException("The key was null");
		}
		
		return this.theMap.containsKey(aKey);
	}

	@Override
	public int size() {
		return this.theMap.size();
	}

	@Override
	public List<ScrambledWord> values() {
		return new ArrayList<ScrambledWord>(this.theMap.values());
	}

	@Override
	public List<String> keys() {
		return new ArrayList<String>(this.theMap.keySet());
	}

}