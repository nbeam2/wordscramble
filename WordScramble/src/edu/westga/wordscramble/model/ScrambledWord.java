package edu.westga.wordscramble.model;

import edu.westga.wordscramble.controllers.WordScramblingController;

/**
 * ScrambledWord objects represent a scrambled and unscrambled word.
 * 
 * @author Nathan Beam
 * @version	Spring, 2015
 */
public class ScrambledWord {

	private String unscrambledWord;
	private String scrambledWord;
	private WordScramblingController newScramble;
	private int wordLength;

	/**
	 * Creates a new ScrambledWord object with the specified
	 * unscrambledWord
	 * 
	 * @precondition 	unscrambledWord != null && 
	 * 						unscrambledWord.length() > 0 
	 * @postcondition	the proper values are assigned
	 * 
	 * @param unscrambledWord			the unscrambled word
	 */
	public ScrambledWord(String unscrambledWord) {
		if (unscrambledWord == null) {
			throw new IllegalArgumentException("Word is null");
		}
		if (unscrambledWord.length() == 0) {
			throw new IllegalArgumentException("Word is empty");
		}
		
		this.unscrambledWord = unscrambledWord;
		this.newScramble = new WordScramblingController(this.unscrambledWord);
		if (this.unscrambledWord.contains(" ")) {
			this.scrambledWord = this.newScramble.doubleScramble();
		} else {
			this.scrambledWord = this.newScramble.getScrambledWord();
		}
		this.wordLength = this.scrambledWord.length();
	}

	/**
	 * Returns the unscrambled word
	 * 
	 * @precondition	none
	 * @return the Unscrambled Word
	 */
	public String getUnscrambledWord() {
		return this.unscrambledWord;
	}

	/**
	 * Returns the scrambled word
	 * 
	 * @precondition	none
	 * @return the Scrambled Word
	 */
	public String getScrambledWord() {
		return this.scrambledWord;
	}
	
	/**
	 * Returns the length of the word
	 * 
	 * @precondition	none
	 * @return the Scrambled Word's length
	 */
	public int getWordLength() {
		return this.wordLength;
	}
}