package edu.westga.wordscramble.view;

import java.io.File;
import java.io.IOException;

import javax.swing.JOptionPane;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

/**
 * GuiCodeBehind defines the JavaFX "controller" for WordScrambleGui.fxml.
 * 
 * @author Nathan Beam
 * @version Spring 2015
 */
public class WordScrambleGuiCodeBehind {

	private static final String READ_DATA_ERROR_MESSAGE = 
			  "ERROR: couldn't load the data";
	private static final String ERROR_DIALOG_TITLE = 
			  "Data import error";
	private static final int EASY_MODE = 5;
	private static final int HARD_MODE = 6;
	private static final int RANDOM_MODE = 0;
	private static final int DOUBLE_SCRAMBLE_MODE = -1;
	
	private WordScrambleViewModel theViewModel;
	
	@FXML
    private Label scrambledWordLabel;
	
	@FXML
	private Label scoreLabel;
	
	@FXML
    private Label correctOrIncorrectLabel;

    @FXML
    private Button submitButton;

    @FXML
    private Button newWordButton;

    @FXML
    private MenuItem loadFromRemoteFileMenuItem;

    @FXML
    private MenuItem loadFromFileMenuItem;

    @FXML
    private MenuItem loadFromWebpageMenuIem;
    
    @FXML
    private MenuItem loadFromRandomWebpageMenuItem;
    
    @FXML
    private MenuItem exitMenuItem;
	
    @FXML
    private TextField userInputField;

    @FXML
    private RadioMenuItem easyModeMenuOption;

    @FXML
    private RadioMenuItem hardModeMenuOption;

    @FXML
    private RadioMenuItem randomModeMenuOption;
    
    @FXML
    private RadioMenuItem doubleScrambleModeMenuOption;
    
    private ToggleGroup toggleGroup;
	
	/**
	* Creates a new MoviesGuiCodeBehind object and its view model.
	* 
	* @precondition none
	* @postcondition the object and its view model are ready to be initialized
	*/
	public WordScrambleGuiCodeBehind() {
		this.theViewModel = new WordScrambleViewModel();
	}
	
	/**
	* Initializes the GUI component, binding them to the view model properties
	* and setting their event handlers.
	*/
	@FXML
	public void initialize() {
		this.bindComponentsToViewModel();
		this.setEventActions();
		this.setToggleGroup();
	}
	
	
	/**
	 * Binds the components to changable properties
	 * 
	 * @postcondition the properties are bound
	 */
	private void bindComponentsToViewModel() {
		this.scrambledWordLabel.textProperty().bindBidirectional(
				  this.theViewModel.scrambledWordProperty());
		this.correctOrIncorrectLabel.textProperty().bindBidirectional(
				  this.theViewModel.correctOrIncorrectProperty());
		this.scoreLabel.textProperty().bindBidirectional(
				  this.theViewModel.scoreProperty());
	}
	
	/**
	 * Sets the event actions for the various gui controls
	 * 
	 * @postcondition the actions are set
	 */
	private void setEventActions() {
		this.newWordButton.setOnAction(event -> this.theViewModel.getNextWord());
		this.submitButton.setOnAction(event -> this.handleSubmitAction());
	
		this.exitMenuItem.setOnAction(event -> Platform.exit());
		this.loadFromFileMenuItem.setOnAction(
					 event -> this.handleLoadAction());	
		this.loadFromRemoteFileMenuItem.setOnAction(
				  event -> this.handleRemoteLoadAction());
		
		this.easyModeMenuOption.setOnAction(
				  event -> this.theViewModel.setGameMode(WordScrambleGuiCodeBehind.EASY_MODE));
		this.hardModeMenuOption.setOnAction(
				  event -> this.theViewModel.setGameMode(WordScrambleGuiCodeBehind.HARD_MODE));
		this.randomModeMenuOption.setOnAction(
				  event -> this.theViewModel.setGameMode(WordScrambleGuiCodeBehind.RANDOM_MODE));
		this.doubleScrambleModeMenuOption.setOnAction(
				  event -> this.theViewModel.setGameMode(WordScrambleGuiCodeBehind.DOUBLE_SCRAMBLE_MODE));
	}
	
	
	/**
	 * Handles the load action for a local file
	 * 
	 * @postcondition the file is loaded into the game
	 */
	private void handleLoadAction() {
		FileChooser chooser = this.initializeFileChooser("Read from");

		File inputFile = chooser.showOpenDialog(null);
		if (inputFile == null) {
			return;
		}
		
		try {
			this.theViewModel.loadWordDataFrom(inputFile);
			this.enableButtons();
		} catch (IOException readException) {
			JOptionPane.showMessageDialog(null,
				    READ_DATA_ERROR_MESSAGE,
				    ERROR_DIALOG_TITLE,
				    JOptionPane.ERROR_MESSAGE);
		}
		
	}
	
	/**
	 * Handles the load action for a remote file
	 * 
	 * @postcondition the file is loaded into the game
	 */
	private void handleRemoteLoadAction() {
		String url = JOptionPane.showInputDialog(null, "Enter URL of file to load", "Remote Load", 1);
		try {
			this.theViewModel.loadRemoteWordDataFrom(url);
			this.enableButtons();
		} catch (IOException readException) {
			JOptionPane.showMessageDialog(null,
				    READ_DATA_ERROR_MESSAGE,
				    ERROR_DIALOG_TITLE,
				    JOptionPane.ERROR_MESSAGE);
		}
	}
	
	
	/**
	 * Handles the preliminary proceedures for checking to see if an 
	 * answer is correct before handing the task off to the viewmodel 
	 * 
	 * @precondition the textfield is not empty
	 * @postcondition none
	 */
	private void handleSubmitAction() {
		String userGuess = this.userInputField.getText().trim();
		this.userInputField.clear();
		if (userGuess.equals("") 
				  || this.userInputField.getText() == null) {
			return;
		}
		this.theViewModel.checkCorrect(userGuess);		
	}
	
	
	/**
	 * Simply sets the buttons clickable.
	 * Called after any file is loaded.
	 * 
	 * @postcondition the buttons can be clicked
	 */
	private void enableButtons() {
		this.newWordButton.setDisable(false);
		this.submitButton.setDisable(false);
	}

	
	/**
	 * Constructs a filechooser window to select a 
	 * local file to load.
	 * 
	 * @precondition none
	 * @postcondition there is a new filechooser created
	 * 
	 * @param title the title of the window
	 * @return A new filechooser
	 */
	private FileChooser initializeFileChooser(String title) {
		FileChooser chooser = new FileChooser();
		chooser.getExtensionFilters().add(
						new ExtensionFilter("Text Files", "*.txt"));
		chooser.setTitle(title);
		return chooser;
	}
	
	
	/**
	 * Handles a press of the enter button in the textfield
	 * to make submitting more convenient
	 * 
	 * @postcondition the user's answer has been submitted
	 */
	@FXML
	private void onEnter() {
		this.handleSubmitAction();
	}
	
	/**
	 * Sets the toggleGroup for the radio menu items in the 
	 * difficulty menu
	 * 
	 * @precondition the gui is initialized
	 * @postcondition the radioMenuItems are bound to the same toggleGroup
	 */
	private void setToggleGroup() {
		this.toggleGroup = new ToggleGroup();
		this.easyModeMenuOption.setToggleGroup(this.toggleGroup);
		this.hardModeMenuOption.setToggleGroup(this.toggleGroup);
		this.randomModeMenuOption.setToggleGroup(this.toggleGroup);
		this.doubleScrambleModeMenuOption.setToggleGroup(this.toggleGroup);
	}

}
