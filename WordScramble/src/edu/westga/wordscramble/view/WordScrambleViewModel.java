package edu.westga.wordscramble.view;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import edu.westga.wordscramble.model.DataMap;
import edu.westga.wordscramble.model.ScrambledWord;
import edu.westga.wordscramble.model.WordDataMap;
import edu.westga.wordscramble.controllers.AbstractLoadWordData;
import edu.westga.wordscramble.controllers.GetNextWordController;
import edu.westga.wordscramble.controllers.LoadWordDataFromLocalFileController;
import edu.westga.wordscramble.controllers.LoadWordDataFromUrlController;

/**
 * ViewModel defines the view model for the wordscramble application.
 * 
 * @author Nathan Beam
 * @version Spring 2015
 */
public class WordScrambleViewModel {

	private AbstractLoadWordData loadController;
	private GetNextWordController getNextWordController;
	private DataMap<String, ScrambledWord> theMap;
	private ArrayList<String> scrambledWordsArrayList;
	private StringProperty scrambledWord;
	private StringProperty correctOrIncorrect;
	private StringProperty scoreStringProperty;
	private String unscrambledWord;
	private int score;
	
	private int gameMode = 0;
	
	private static final String CORRECT_ANSWER_MESSAGE = 
			  "That is correct!";
	private static final String INCORRECT_ANSWER_MESSAGE = 
			  "Sorry, try again!";
	private static final int POINTS_PER_LETTER = 10;
	
	
	/**
	 * Creates a new WordScrambleViewModel
	 * 
	 * @precondition none
	 * @postcondition a new WordScrambleViewModel is created
	 */
	public WordScrambleViewModel() {
		this.theMap = new WordDataMap();
		this.scrambledWord = new SimpleStringProperty(" ");
		this.correctOrIncorrect = new SimpleStringProperty("");
		this.score = 0;
		this.scoreStringProperty = new SimpleStringProperty(this.score + ""); 
	}
	
	/**
	 * Tells the LoadWordDataFromLocalFileController to import word data from the
	 * specified file. 
	 * 
	 * @precondition	inputFile != null
	 * @postcondition	the data has been loaded 					
	 * 
	 * @param inputFile	the file to read
	 * @throws IOException 	if the file cannot be read
	 */
	public void loadWordDataFrom(File inputFile) throws IOException {
		if (inputFile == null) {
			throw new IllegalArgumentException("File to load is null");
		}
		
		this.loadController = new LoadWordDataFromLocalFileController(inputFile, this.theMap);
		this.loadController.loadWordData();
		this.scrambledWordsArrayList = this.loadController.getUnscrambledWordsArrayList();
		this.getNextWordController = new GetNextWordController(this.theMap, this.scrambledWordsArrayList);
	}
	
	/**
	 * Tells the LoadWordDataFromRemoteFileController to import word data from the
	 * specified file. 
	 * 
	 * @precondition	urlToLoad != null
	 * @postcondition	the data has been loaded 					
	 * 
	 * @param urlToLoad	the url of the file to read
	 * @throws IOException 	if the file cannot be read
	 */
	public void loadRemoteWordDataFrom(String urlToLoad) throws IOException {
		if (urlToLoad == null || urlToLoad.equals("")) {
			throw new IllegalArgumentException("File to load is null");
		}
		
		this.loadController = new LoadWordDataFromUrlController(urlToLoad, this.theMap);
		this.loadController.loadWordData();
		this.scrambledWordsArrayList = this.loadController.getUnscrambledWordsArrayList();
		this.getNextWordController = new GetNextWordController(this.theMap, this.scrambledWordsArrayList);
	}
	
	/**
	 * Returns the property that wraps the scrambled word
	 * 
	 * @precondition none
	 * @return the scrambled word string property
	 */
	public StringProperty scrambledWordProperty() {
		return this.scrambledWord;
	}
	
	/**
	 * Returns the property that wraps the score
	 * 
	 * @precondition none
	 * @return the score string property
	 */
	public StringProperty scoreProperty() {
		return this.scoreStringProperty;
	}
	
	/**
	 * Returns the property that wraps the property
	 * notifying the player whether their choice was correct
	 * 
	 * @precondition none
	 * @return the correct or incorrect string property
	 */
	public StringProperty correctOrIncorrectProperty() {
		return this.correctOrIncorrect;
	}
	
	/**
	 * Sets the required length of the words
	 * 
	 * @param gameMode the length of the words to 
	 * 					  be scrambled 
	 */
	public void setGameMode(int gameMode) {
		this.gameMode = gameMode;
	}
	
	/**
	 * Requests the next ScrambledWord from
	 * the GetNextScrambledWordController based
	 * on the current GameMode
	 * 
	 * @precondition the user has clicked "New Scrambled word" button
	 * @postcondition a new scrambled word is displayed
	 */
	public void getNextWord() {
		ScrambledWord nextWord = this.getNextWordController.getNextWord(this.gameMode);
		this.scrambledWord.setValue(nextWord.getScrambledWord());
		this.unscrambledWord = nextWord.getUnscrambledWord();
		this.correctOrIncorrect.setValue("");
	}
	
	/**
	 * Checks to see if a user correctly guessed the 
	 * unscrambled word
	 * 
	 * @param userGuess String value of the user's guess
	 * 
	 * @precondition userguess != null (assumed false if so)
	 * @postcondition the proper confirmation is displayed,
	 * 					points are added if the user was correct
	 */
	public void checkCorrect(String userGuess) {
		if (userGuess.equalsIgnoreCase(this.unscrambledWord)) {
			this.addPoints();
			this.correctOrIncorrect.setValue(WordScrambleViewModel.CORRECT_ANSWER_MESSAGE);
			this.scrambledWord.setValue(this.unscrambledWord);
		} else {
			this.correctOrIncorrect.setValue(INCORRECT_ANSWER_MESSAGE);
		}
	}
	
	/**
	 * Adds the specified number of points based on the length of 
	 * the word
	 * 
	 * @precondition the user has successfully guessed a word
	 * @postcondition the score is updated accordingly
	 */
	private void addPoints() {
		this.score += this.unscrambledWord.length()
					 * WordScrambleViewModel.POINTS_PER_LETTER;	
		this.scoreStringProperty.setValue(this.score + "");
	}
}
