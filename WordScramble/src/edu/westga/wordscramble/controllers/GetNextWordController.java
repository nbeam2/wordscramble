package edu.westga.wordscramble.controllers;

import java.util.ArrayList;
import java.util.Random;

import edu.westga.wordscramble.model.DataMap;
import edu.westga.wordscramble.model.ScrambledWord;

/**
 * GetNextWordController handles the fetching of a 
 * new ScrambledWord from the DataMap. 
 * 
 * @author Nathan Beam
 * @version Spring 2015 
 *
 */
public class GetNextWordController {

	private DataMap<String, ScrambledWord> theMap;
	private ArrayList<String> scrambledWordsArrayList;
	
	
	/**
	 * Creates a new GetNextWord controller
	 * 
	 * @param aMap the datamap containing String/ScrambledWord pairs
	 * @param scrambledWordsArrayList The arraylist containing 
	 * 									Strings to use as keys
	 * 
	 * @precondition none
	 * @postcondition a new GetNextWordController object is initialized
	 */
	public GetNextWordController(DataMap<String, ScrambledWord> aMap, ArrayList<String> scrambledWordsArrayList) {
		this.theMap = aMap;
		this.scrambledWordsArrayList = scrambledWordsArrayList;
	}
	
	/**
	 * If in easy or hard mode, generate
	 * words until one of the proper length
	 * is found. Else, generate a random 
	 * word from the ArrayList
	 * 
	 * @precondition there is a file loaded
	 * @postcondition a new word is set
	 * 
	 * @param gameMode the current gameMode
	 * 			(5 = easy, 6 = hard, 0 = random, -1 = double)
	 * 
	 * @return the next word to use
	 */
	public ScrambledWord getNextWord(int gameMode) {
		ScrambledWord newWord = this.generateWord();
		if (gameMode == 0) {
			return newWord;
		} else if (gameMode == -1) { 
			newWord = new ScrambledWord(newWord.getUnscrambledWord() + " " + this.generateWord().getUnscrambledWord());
			return newWord;
		} else {	
			while (newWord.getWordLength() 
					!= gameMode) {
				newWord = this.generateWord();
			}
			return newWord;
		}
	}
	
	/**
	 * Generates a new random within the bounds of the 
	 * ArrayList and uses the indexed value as the key for the 
	 * datamap containing the scrambled word
	 * 
	 * @precondition there is a file loaded
	 * @postcondition a new word is displayed for the user to unscramble
	 */
	private ScrambledWord generateWord() {
		Random rand = new Random();
		int random = rand.nextInt(this.scrambledWordsArrayList.size());
		return this.theMap.get(this.scrambledWordsArrayList.get(random));
	}
	
	
}
