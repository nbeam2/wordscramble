package edu.westga.wordscramble.controllers;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import edu.westga.wordscramble.datatier.TextUrlLoader;
import edu.westga.wordscramble.model.DataMap;
import edu.westga.wordscramble.model.ScrambledWord;

/**
 * LoadWordDataFromUrlController converts a string to a url resource and instructs a
 * TextUrlLoader to load the words from the url
 * 
 * @author Nathan Beam
 * @version Spring 2015
 */
public class LoadWordDataFromUrlController extends AbstractLoadWordData {

	private URL urlToLoad;


	/**
	 * Creates a new LoadWordDataFromUrlController for input of the specified web resource.
	 * 
	 * @precondition stringToConvert != null
	 * @postcondition the words will be loaded into the datamap
	 * 
	 * @param stringToConvert	the string of the web resource to input
	 * @param aMap 				the datamap in which to store words
	 */
	public LoadWordDataFromUrlController(String stringToConvert, DataMap<String, ScrambledWord> aMap) {
		super(aMap);
		if (stringToConvert == null) {
			throw new IllegalArgumentException("Url to load is null!");
		}
		
		try {
			this.urlToLoad = new URL(stringToConvert);
		} catch (MalformedURLException exception) {

			exception.printStackTrace();
		}

		TextUrlLoader turl = new TextUrlLoader(this.urlToLoad);

		try {
			super.setLinesFromFile(turl.loadText());
		} catch (IOException exception) {
			// TODO Auto-generated catch block
			exception.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see edu.westga.wordscramble.controllers.AbstractLoadWordData#loadWordData()
	 */
	@Override
	public void loadWordData() throws IOException {	
		super.loadWordData();		
	}
	
	/* (non-Javadoc)
	 * @see edu.westga.wordscramble.controllers.AbstractLoadWordData#getScrambledWordsArrayList()
	 */
	@Override
	public ArrayList<String> getUnscrambledWordsArrayList() {
		return super.getUnscrambledWordsArrayList();
	}
	
}

