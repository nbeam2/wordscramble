package edu.westga.wordscramble.controllers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import edu.westga.wordscramble.datatier.TextFileLoader;
import edu.westga.wordscramble.datatier.TextLoader;
import edu.westga.wordscramble.model.DataMap;
import edu.westga.wordscramble.model.ScrambledWord;

/**
 * LoadWordDataFromLocalFileController manages the "load word data from file" use case:
 * reading word data from a text file and storing it in the data map.
 * 
 * @author Nathan Beam
 * @version Spring 2015
 */
public class LoadWordDataFromLocalFileController extends AbstractLoadWordData {


	/**
	 * Creates a new LoadWordDataFromLocalFileController to manage adding words
	 * to the specified map.
	 * 
	 * @precondition inputFile != null && aMap != null
	 * @postcondition the controller is ready to load words
	 * 
	 * @param inputFile
	 *            the file to read
	 * @param aMap
	 *            the data map to which words will be added
	 */
	public LoadWordDataFromLocalFileController(File inputFile, DataMap<String, ScrambledWord> aMap) {
		super(aMap);
		
		if (inputFile == null) {
			throw new IllegalArgumentException("File to load is null");
		}		
		TextLoader loader = new TextFileLoader(inputFile);
		try {
			super.setLinesFromFile(loader.loadText());
		} catch (IOException exception) {
			// TODO Auto-generated catch block
			exception.printStackTrace();
		}	
	}
	
	/* (non-Javadoc)
	 * @see edu.westga.wordscramble.controllers.AbstractLoadWordData#loadWordData()
	 */
	@Override
	public void loadWordData() throws IOException {
		super.loadWordData();
	}
	
	/* (non-Javadoc)
	 * @see edu.westga.wordscramble.controllers.AbstractLoadWordData#getScrambledWordsArrayList()
	 */
	@Override
	public ArrayList<String> getUnscrambledWordsArrayList() {
		return super.getUnscrambledWordsArrayList();
	}
	

	
	
}
