package edu.westga.wordscramble.controllers;

import java.util.ArrayList;
import java.util.Collections;

/**
 * WordScramblingController handles the Scrambling words usecase
 * 
 * @author Nathan Beam
 * @version Spring 2015
 */
public class WordScramblingController {

	private String wordToScramble;

	/**
	 * Scrambles a word from the specified wordToScamble
	 * 
	 * @precondition wordToScamble != null && wordToScamble.length() > 0
	 * @postcondition the data is initialized
	 * 
	 * @param wordToScramble
	 *            the unscrambled word
	 */
	public WordScramblingController(String wordToScramble) {
		if (wordToScramble == null) {
			throw new IllegalArgumentException("Name is null");
		}
		if (wordToScramble.length() == 0) {
			throw new IllegalArgumentException("Name is empty");
		}

		this.wordToScramble = wordToScramble;
	}

	/**
	 * Converts the String into a char array, then the char array into an
	 * ArrayList, shuffles the ArrayList, the concatenates the string with a for
	 * each loop. Head Hurt yet?
	 * 
	 * @return the scrambled word
	 */
	public String getScrambledWord() {
		ArrayList<Character> scrambledArrayList = new ArrayList<Character>();
		for (char character : this.wordToScramble.toCharArray()) {
			scrambledArrayList.add(character);
		}
		Collections.shuffle(scrambledArrayList);
		String scrambledWord = "";
		for (char character : scrambledArrayList) {
			scrambledWord += character;
		}
		return scrambledWord;
	}

	/**
	 * Handles the (horrifying) usecase of scrambling two words together
	 * <p>
	 * (Note: this would have been a simple mater of concatenating two 
	 * words together and rescrambling, but I wanted to have a space in the 
	 * string where there would be if the words were descrambled. I felt like
	 * 2 scrambled words of unknown length was too cruel.
	 * 
	 * @precondition none
	 * @postcondition the doubleword has been scrambled
	 * 
	 * @return the scrambled doubleword
	 */
	public String doubleScramble() {

		String[] wordSplit = this.wordToScramble.split(" ");
		String word1 = wordSplit[0];
		String word2 = wordSplit[1];
		ArrayList<Character> scrambledArrayList = new ArrayList<Character>();

		for (char character : word1.toCharArray()) {
			scrambledArrayList.add(character);
		}

		int spaceIndex = scrambledArrayList.size();

		for (char character : word2.toCharArray()) {
			scrambledArrayList.add(character);
		}
		Collections.shuffle(scrambledArrayList);

		String scrambledWord = "";
		for (char character : scrambledArrayList) {
			scrambledWord += character;
		}
		scrambledWord = new StringBuilder(scrambledWord)
				.insert(spaceIndex, " ").toString();
		return scrambledWord;
	}
}
