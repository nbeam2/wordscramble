package edu.westga.wordscramble.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import edu.westga.wordscramble.model.DataMap;
import edu.westga.wordscramble.model.ScrambledWord;

/**
 * AbstactLoadData defines an abstract outline of 
 * the load text from a file usecase. After execution, 
 * the datamap and ArrayList will be populated with 
 * words to use in the game.
 * 
 * @author Nathan Beam 
 * @version Spring 2015
 *
 */
public class AbstractLoadWordData {

	private ArrayList<String> unscrambledWordsArrayList;
	private List<String> linesFromFile;
	private DataMap<String, ScrambledWord> theMap;
	private static final int MIN_WORD_LENGTH = 4;

	/**
	 * Creates a new AbstractLoadWordDataInstance
	 * 
	 * @precondition aMap != null
	 * @postcondition object is initialized
	 * 
	 * @param aMap the datamap to which to add the words
	 */
	public AbstractLoadWordData(DataMap<String, ScrambledWord> aMap) {
		if (aMap == null) {
			throw new IllegalArgumentException("Map to load is null");
		}

		this.unscrambledWordsArrayList = new ArrayList<String>();
		this.theMap = aMap;
 	}

	/**
	 * Loads movie data from the input file into the map.
	 * 
	 * @precondition none
	 * @postcondition the map contains the movie data
	 * 
	 * @throws IOException
	 *             if the file cannot be read
	 */
	public void loadWordData() throws IOException {	
		for (String oneLineFromFile : this.linesFromFile) {
			String[] wordArray = oneLineFromFile.split(" ");
	        this.sanitizeLine(wordArray);
		}
	}
	
	/**
	 * This function only exists because I can't nest for loops
	 * 
	 * @precondition the wordarray is not null
	 * @postcondition the words are added to the map and arraylist
	 * 
	 * @param wordArray the array to iterate over and add
	 */
	private void sanitizeLine(String[] wordArray) {
		if (wordArray == null || wordArray.length == 0) {
			throw new IllegalArgumentException("Wordarray add from is empty");
		}
		for (String word : wordArray) {
        	String sanitized = word.replaceAll("[^a-zA-Z]", "");
        	sanitized = sanitized.toLowerCase();
        	if (word.length() >= AbstractLoadWordData.MIN_WORD_LENGTH) {
    		    this.addWord(sanitized);

        	}
		}
	}

	/**
	 * Returns the ArrayList of words that corresponds with the datamap
	 * 
	 * @return the arraylist of words
	 */
	public ArrayList<String> getUnscrambledWordsArrayList() {
		return this.unscrambledWordsArrayList;
	}

	
	/**
	 * Adds words from the file to the Arraylist
	 * 
	 * @precondition word != null && word.length > 1
	 * @postcondition the word is added to the arraylist 
	 * 
	 * @param word
	 */
	private void addWord(String word) {
		if (word == null || word.equals("")) {
			throw new IllegalArgumentException("Word To add is empty");
		}
		if (!this.theMap.contains(word)) {
			ScrambledWord scrambledWord = new ScrambledWord(word);
			this.theMap.add(scrambledWord.getUnscrambledWord(), scrambledWord);
			this.unscrambledWordsArrayList.add(scrambledWord.getUnscrambledWord());
		}
	}

	/**
	 * Sets the lines from file pulled by the subclass
	 * 
	 * @param linesFromFile the linesFromFile to set
	 */
	protected void setLinesFromFile(List<String> linesFromFile) {
		if (linesFromFile == null) {
			throw new IllegalArgumentException("Lines to add are null");
		}
		this.linesFromFile = linesFromFile;
	}
}