package edu.westga.wordscramble.controllers;

import java.io.IOException;
import java.util.ArrayList;


/**
 * LoadWordData defines the usecase of loading 
 * words from a file, either local or remote
 * 
 * @author Nathan Beam
 * @version Spring 2015
 *
 */
public interface LoadWordData {
	
	/**
	 * Loads word data from the input into the map.
	 * 
	 * @precondition none
	 * @postcondition the map contains the word data
	 * 
	 * @throws IOException
	 *             if the file cannot be read
	 */
	void loadWordData() throws IOException;
	
	
	/**
	 * Returns the ArrayList of scrambled words
	 * 
	 * @return scrambled words ArrayList
	 */
	ArrayList<String> getScrambledWordsArrayList();

	/**
	 * Adds a word to the datamap and ArrayList
	 * 
	 * @param word the word to add
	 */
	void addWord(String word);
}