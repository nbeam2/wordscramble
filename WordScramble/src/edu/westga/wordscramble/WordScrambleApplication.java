package edu.westga.wordscramble;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;


/**
 * MoviesApplication extends the JavaFX Application class to build the GUI
 * and start program execution.
 * 
 * @author CS 1302
 * @version Spring, 2015
 */
public class WordScrambleApplication extends Application {

	private static final String WINDOW_TITLE = "Word Scramble!";
	private static final String GUI_FXML = "view/WordScrambleGui.fxml";

	/**
	 * Constructs a new MoviesApplication object that extends the
	 * Application class to build the GUI and start program execution.
	 * 
	 * @precondition none
	 * @postcondition the object is ready to execute.
	 */
	public WordScrambleApplication() {
		super();
	}

	@Override
	public void start(Stage primaryStage) {
		try {
			Pane pane = this.loadGui();
			Scene scene = new Scene(pane);
			primaryStage.setScene(scene);
			primaryStage.setTitle(WINDOW_TITLE);
			primaryStage.show();

		} catch (IllegalStateException | IOException anException) {
			anException.printStackTrace();
		}

	}

	private Pane loadGui() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource(GUI_FXML));
		return (Pane) loader.load();
	}

	/**
	 * Launches the application.
	 * 
	 * @param args
	 *            not used
	 */
	public static void main(String[] args) {
		launch(args);
	}

}
