package edu.westga.wordscramble.datatier;

import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.io.*;

/**
 * TextUrlLoader loads the contents of a specified web resource into memory.
 * 
 * @author CS 1302
 * @version Spring, 2015
 */
public class TextUrlLoader implements TextLoader {

	private URL textResource;

	/**
	 * Creates a new TextUrlLoader for input of the specified web resource.
	 * 
	 * @precondition aUrl != null
	 * @postcondition the object is ready to read the resource
	 * 
	 * @param aUrl	the URL of the web resource to input
	 */
	public TextUrlLoader(URL aUrl) {
		if (aUrl == null) {
			throw new IllegalArgumentException("URL to input is null");
		}
		
		this.textResource = aUrl;
	}

	/**
	 * Reads all lines from the data source.
	 * 
	 * @precondition none
	 * @return a list of Strings containing the contents of the data source
	 * 
	 * @throws IOException
	 *             if the data source can't be read
	 */
	public List<String> loadText() throws IOException {
		List<String> textLines = new ArrayList<>();
		
		Scanner pageScanner = new Scanner(this.textResource.openStream());
		while (pageScanner.hasNext()) {
			textLines.add(pageScanner.nextLine());
		}
		pageScanner.close();

		return textLines;
	}

}